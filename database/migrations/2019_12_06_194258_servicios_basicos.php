<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ServiciosBasicos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes.servicios_basicos', function (Blueprint $table) {
            $table->bigIncrements('sba_id');
            $table->char('sba_codigo');
            $table->text('sba_nombres');
            $table->text('sba_descripcion');
            $table->date('sba_fecha_ini');
            $table->date('sba_fecha_fin');
            $table->string('sba_nro_nuri');
            $table->timestamp('sba_registrado')->default(DB::raw('CURRENT_TIMESTAMP'));            
            $table->timestamp('sba_modificado')->default(DB::raw('CURRENT_TIMESTAMP'));   
            $table->integer('sba_usr_id');                     
            $table->char('sba_estado',1)->default('A');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes.servicios_basicos');
    }
}
